
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import no.ntnu.idatt2001.CardHand;
import no.ntnu.idatt2001.PlayingCard;

public class CardHandTest {

    @Test
    public void getHandTest() {
        CardHand cardHand = new CardHand();

        int expected = 0;
        int actual = cardHand.getHand().size();
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test that the hand is updated")
    public void updateCardHandTest() {
        CardHand cardHand = new CardHand();

        int expected = 5;

        cardHand.updateCardHand();
        int actual = cardHand.getHand().size();
        assertEquals(expected, actual);
    }

    @Test
    public void sumOfFacesTest() {
        CardHand cardHand = new CardHand();

        PlayingCard playingCard = new PlayingCard('S', 3);
        PlayingCard playingCard2 = new PlayingCard('S', 3);
        PlayingCard playingCard3 = new PlayingCard('S', 3);
        PlayingCard playingCard4 = new PlayingCard('S', 3);
        PlayingCard playingCard5 = new PlayingCard('S', 3);

        cardHand.getHand().add(playingCard);
        cardHand.getHand().add(playingCard2);
        cardHand.getHand().add(playingCard3);
        cardHand.getHand().add(playingCard4);
        cardHand.getHand().add(playingCard5);

        int expected = 15;
        int actual = cardHand.sumOfFaces();
        assertEquals(expected, actual);
    }

    @Test
    public void hasThreeOfAKindTestTrue() {
        CardHand cardHand = new CardHand();

        PlayingCard playingCard = new PlayingCard('H', 3);
        PlayingCard playingCard2 = new PlayingCard('H', 3);
        PlayingCard playingCard3 = new PlayingCard('H', 3);
        PlayingCard playingCard4 = new PlayingCard('S', 3);
        PlayingCard playingCard5 = new PlayingCard('S', 3);

        cardHand.getHand().add(playingCard);
        cardHand.getHand().add(playingCard2);
        cardHand.getHand().add(playingCard3);
        cardHand.getHand().add(playingCard4);
        cardHand.getHand().add(playingCard5);

        boolean expected = false;
        boolean actual = cardHand.hasThreeOfAKind();
        assertEquals(expected, actual);
    }

    @Test
    public void hasThreeOfAKindTestFalse() {
        CardHand cardHand = new CardHand();

        PlayingCard playingCard = new PlayingCard('S', 3);
        PlayingCard playingCard2 = new PlayingCard('S', 3);
        PlayingCard playingCard3 = new PlayingCard('S', 3);
        PlayingCard playingCard4 = new PlayingCard('S', 3);
        PlayingCard playingCard5 = new PlayingCard('S', 3);

        cardHand.getHand().add(playingCard);
        cardHand.getHand().add(playingCard2);
        cardHand.getHand().add(playingCard3);
        cardHand.getHand().add(playingCard4);
        cardHand.getHand().add(playingCard5);

        boolean expected = false;
        boolean actual = cardHand.hasThreeOfAKind();
        assertEquals(expected, actual);
    }

    @Test
    public void getSuitTest() {
        CardHand cardHand = new CardHand();

        PlayingCard playingCard = new PlayingCard('D', 3);
        PlayingCard playingCard2 = new PlayingCard('D', 3);
        PlayingCard playingCard3 = new PlayingCard('D', 3);
        PlayingCard playingCard4 = new PlayingCard('D', 3);
        PlayingCard playingCard5 = new PlayingCard('S', 3);

        cardHand.getHand().add(playingCard);
        cardHand.getHand().add(playingCard2);
        cardHand.getHand().add(playingCard3);
        cardHand.getHand().add(playingCard4);
        cardHand.getHand().add(playingCard5);

        List<PlayingCard> actual = cardHand.getSuit('D');
        PlayingCard expected = playingCard;

        assertEquals(expected, actual.get(0));

    }

}

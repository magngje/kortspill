
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.ntnu.idatt2001.DeckOfCards;

public class DeckOfCardsTest {

    DeckOfCards deck = new DeckOfCards();

    @Test
    public void dealHandTest() {
        int n = 5;
        int expected = 5;
        int actual = deck.dealHand(n).size();

        deck.dealHand(n);
        assertEquals(expected, actual);

    }

    public void createDeckTest() {
        int expected = 52;
        int actual = deck.createDeck().size();
        assertEquals(expected, actual);
    }

    public void getDeckTest() {
        int expected = 52;
        int actual = deck.getDeck().size();
        assertEquals(expected, actual);
    }

}

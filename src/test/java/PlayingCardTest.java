
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.ntnu.idatt2001.PlayingCard;

public class PlayingCardTest {

    PlayingCard playingCard = new PlayingCard('S', 4);

    @Test
    void getAsString() {
        String expected = "S4";
        String actual = playingCard.getAsString();
        assertEquals(expected, actual);

    }

    @Test
    void getSuit() {
        char expected = 'S';
        char actual = playingCard.getSuit();
        assertEquals(expected, actual);
    }

    @Test
    void getFace() {
        int expected = 4;
        int actual = playingCard.getFace();
        assertEquals(expected, actual);
    }

}

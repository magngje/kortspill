package no.ntnu.idatt2001;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DeckOfCards {

    private final char[] suit = { 'S', 'H', 'D', 'C' };

    private final int[] face = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    // private PlayingCard[] deck = new PlayingCard[52];

    private List<PlayingCard> deck = new java.util.ArrayList<>();

    public DeckOfCards() {
        deck = createDeck();
    }

    public List<PlayingCard> createDeck() {
        for (char s : suit) {
            for (int f : face) {
                deck.add(new PlayingCard(s, f));
            }
        }

        return deck;
    }

    public List<PlayingCard> getDeck() {
        return deck;
    }

    public List<PlayingCard> dealHand(int n) {

        Random r = new Random();
        List<PlayingCard> deck = getDeck();

        /**
         * Create a stream of random integers between 0 and the size of the deck. Then
         * limit the stream to n elements. Then map the stream to the PlayingCard
         * objects in the deck. Finally collect the stream into a list.
         */
        List<PlayingCard> hand = r.ints(0, deck.size())
                .distinct()
                .limit(n)
                .mapToObj(deck::get)
                .collect(Collectors.toList());

        return hand;

    }

}

package no.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CardHand {

    private List<PlayingCard> hand;
    private DeckOfCards deck = new DeckOfCards();

    public CardHand() {
        hand = new ArrayList<>();
    }

    public void updateCardHand() {
        hand = deck.dealHand(5);
    }

    public List<PlayingCard> getHand() {
        return hand;
    }

    public int sumOfFaces() {
        return hand
                .stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    public Boolean hasThreeOfAKind() {
        return hand
                .stream()
                .map(PlayingCard::getFace)
                .distinct()
                .count() == 3;
    }

    public List<PlayingCard> getSuit(char suit) {

        List<PlayingCard> s = hand
                .stream()
                .filter(card -> card.getSuit() == suit)
                .collect(Collectors.toList());

        if (s.isEmpty()) {
            return null;
        } else {
            return s;
        }
    }

    public boolean hasFlush() {
        /**
         * Create a stream of the suits in the hand. Then use the distinct() method to
         * remove duplicates. Finally count the number of elements in the stream. If the
         * number of elements is 1, the hand has a flush.
         */
        return hand
                .stream()
                .map(PlayingCard::getSuit)
                .distinct()
                .count() == 1;
    }

    public boolean hasPair() {
        /**
         * Create a stream of the faces in the hand. Then use the distinct() method to
         * remove duplicates. Finally count the number of elements in the stream. If the
         * number of elements is 4, the hand has a pair.
         */
        return hand
                .stream()
                .map(PlayingCard::getFace)
                .distinct()
                .count() == 4;
    }

    public boolean hasQueenOfSpades() {
        /**
         * Create a stream of the cards in the hand. Then use the anyMatch() method to
         * check if the hand contains a queen of spades.
         */
        return hand
                .stream()
                .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

}

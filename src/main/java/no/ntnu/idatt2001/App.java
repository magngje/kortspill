package no.ntnu.idatt2001;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * JavaFX App
 */
public class App extends Application {

    private static CardHand hand = new CardHand();
    private static Boolean start = false;
    private static Boolean show = false;

    private static int size = 200;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Kortspill");
        AnchorPane root = new AnchorPane();

        Button btn = new Button();
        btn.setStyle("-fx-background-color: #fab152; -fx-text-fill: #000000; -fx-font: 24 arial;");

        btn.setText("Deal cards");
        Button checkHand = new Button("Check hand");
        checkHand.setStyle("-fx-background-color: #fab152; -fx-text-fill: #000000; -fx-font: 24 arial;");

        btn.setOnAction(e -> {
            show = false;

            hand = new CardHand();
            hand.updateCardHand();
            Label label = new Label("Your deck was updated:");
            Label fade = fade(label);

            ImageView shrek = shrek();
            root.getChildren().add(shrek);

            label.setStyle("-fx-font: 24 arial; -fx-text-fill: white; -fx-background-color: black; -fx-padding: 40px;");

            AnchorPane.setRightAnchor(fade, 100.0);
            AnchorPane.setTopAnchor(fade, 300.0);
            root.getChildren().add(fade);
            root.getChildren().add(flowPane());

        });

        checkHand.setOnAction(e -> {
            show = true;
            start = true;
            root.getChildren().remove(flowPane());
            root.getChildren().add(flowPane());

            root.getChildren().remove(stats());
            root.getChildren().add(stats());

        });

        AnchorPane.setRightAnchor(btn, 100.0);
        AnchorPane.setTopAnchor(btn, 100.0);
        AnchorPane.setRightAnchor(checkHand, 100.0);
        AnchorPane.setTopAnchor(checkHand, 200.0);

        root.getChildren().addAll();
        root.getChildren().add(btn);
        root.getChildren().add(checkHand);
        root.getChildren().add(flowPane());
        root.getChildren().add(stats());

        primaryStage.setScene(new Scene(root, 1000, 600));
        primaryStage.show();
    }

    public Label fade(Label label) {
        FadeTransition ft = new FadeTransition(Duration.millis(3000), label);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.play();
        return label;
    }

    public ImageView shrek() {
        FileInputStream inputstream;
        try {
            inputstream = new FileInputStream("src/main/resources/no/ntnu/idatt2001/shrek.png");
            Image image = new Image(inputstream);
            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(size);
            imageView.setFitWidth(size);
            imageView.setPreserveRatio(true);
            imageView.setLayoutX(300);
            imageView.setLayoutY(80);

            size += 50;
            FadeTransition ft = new FadeTransition(Duration.millis(3000), imageView);
            ft.setFromValue(1.0);
            ft.setToValue(0.0);
            ft.play();
            return imageView;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

    public FlowPane stats() {

        FlowPane grid = new FlowPane();
        grid.setStyle("-fx-background-color: black;");
        grid.setHgap(20);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 20, 20, 20));

        grid.setMaxSize(500, 500);

        Label SumOfCards = new Label("Sum of cards: " + hand.sumOfFaces());
        Label QueenOfSpades = new Label("Has queen of spades: " + hand.hasQueenOfSpades());
        Label flush = new Label("Has flush: " + hand.hasFlush());
        Label pair = new Label("Has pair: " + hand.hasPair());
        Label threeOfAKind = new Label("Has three of a kind: " + hand.hasThreeOfAKind());

        SumOfCards.setStyle("-fx-font: 24 arial; -fx-text-fill: white;");
        QueenOfSpades.setStyle("-fx-font: 24 arial; -fx-text-fill: white;");
        flush.setStyle("-fx-font: 24 arial; -fx-text-fill: white;");
        pair.setStyle("-fx-font: 24 arial; -fx-text-fill: white;");
        threeOfAKind.setStyle("-fx-font: 24 arial; -fx-text-fill: white;");

        grid.getChildren().addAll(SumOfCards, QueenOfSpades, flush, pair, threeOfAKind);

        grid.setLayoutX(0);
        grid.setLayoutY(67);

        return grid;
    }

    public FlowPane flowPane() {
        FlowPane grid = new FlowPane();
        grid.setStyle("-fx-background-color: #fab152;");
        grid.setHgap(20);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 20, 20, 20));

        if (!start) {
            Text text = new Text("Click deal cards to start");
            text.setStyle("-fx-font: 24 arial;");
            grid.getChildren().add(text);
            return grid;

        }
        if (!show) {
            Text text = new Text("Click check hand to see your hand");
            text.setStyle("-fx-font: 24 arial;");
            grid.getChildren().add(text);
            return grid;
        }

        Text text = new Text("Your hand:");
        text.setStyle("-fx-font: 24 arial;");
        grid.getChildren().add(text);
        grid.getChildren().add(showHboxHand());

        HBox showHboxHand = showHboxHand();
        showHboxHand.setSpacing(15);

        return grid;
    }

    public HBox showHboxHand() {
        HBox hBox = new HBox();
        hBox.setSpacing(15);

        hand.getHand().stream().forEach(card -> {
            hBox.getChildren().add(PlayingCardView(card));
        });
        return hBox;
    }

    public Text PlayingCardView(PlayingCard card) {
        Text text = new Text(card.getAsString());
        text.setStyle("-fx-font: 24 arial;");
        return text;
    }

    public static void main(String[] args) {
        launch();
    }

}